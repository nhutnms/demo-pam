package com.myspace.loanapprovaldemo;

/**
 * This class was automatically generated by the data modeler tool.
 */

public class AppraisalInformation implements java.io.Serializable {

	static final long serialVersionUID = 1L;

	@org.kie.api.definition.type.Label(value = "ApprovedLoanAmount")
	private java.math.BigInteger approvedLoanAmount;
	@org.kie.api.definition.type.Label(value = "approvedTerm")
	private java.lang.Integer approvedTerm;
	@org.kie.api.definition.type.Label(value = "ManagerComment")
	private java.lang.String managerComment;

	public AppraisalInformation() {
	}

	public java.math.BigInteger getApprovedLoanAmount() {
		return this.approvedLoanAmount;
	}

	public void setApprovedLoanAmount(java.math.BigInteger approvedLoanAmount) {
		this.approvedLoanAmount = approvedLoanAmount;
	}

	public java.lang.Integer getApprovedTerm() {
		return this.approvedTerm;
	}

	public void setApprovedTerm(java.lang.Integer approvedTerm) {
		this.approvedTerm = approvedTerm;
	}

	public java.lang.String getManagerComment() {
		return this.managerComment;
	}

	public void setManagerComment(java.lang.String managerComment) {
		this.managerComment = managerComment;
	}

	public AppraisalInformation(java.math.BigInteger approvedLoanAmount,
			java.lang.Integer approvedTerm, java.lang.String managerComment) {
		this.approvedLoanAmount = approvedLoanAmount;
		this.approvedTerm = approvedTerm;
		this.managerComment = managerComment;
	}

}